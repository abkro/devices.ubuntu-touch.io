---
codename: 'yggdrasil'
name: 'VollaPhone'
icon: 'phone'
image: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1584639699/gybpyos99cvoqjyslkwb.jpg'
video: 'https://www.youtube.com/embed/cSRkhPZlT_I'
maturity: 1
---

The [VollaPhone](https://www.volla.online) is an upcoming commercial flagship device with Ubuntu Touch!
