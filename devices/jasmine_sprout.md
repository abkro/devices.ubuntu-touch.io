---
codename: 'jasmine_sprout'
name: 'Xiaomi Mi A2'
comment: 'wip'
icon: 'phone'
noinstall: true
maturity: 0.8
---

[Xiaomi Mi A2](https://wiki.postmarketos.org/images/6/6a/Xiaomi-jasmine.jpg)

### Device specifications
|    Component | Details                                                         |
|-------------:|-----------------------------------------------------------------|
|      Chipset | Qualcomm SDM665 Snapdragon 660                                  |
|          CPU | Octa-core (4x2.2 GHz Kryo 260 Gold & 4x1.8 GHz Kryo 260 Silver) |
| Architecture | arm64                                                           |
|          GPU | Adreno 512                                                      |
|      Display | 1080x2160                                                       |
|      Storage | 32GB / 64 GB / 128 GB                                           |
| Shipped Android Version | 8.1 (Oreo)                                           |
|       Memory | 4 GB / 6 GB                                                     |
|      Cameras | 20 MP + 12 MP, LED flash<br>20 MP, LED flash                    |
|      Battery | 3000mAh                                                         |
|   Dimensions | 158.7 x 75.4 x 7.3 mm                                           |
|       Weight | 166 g                                                           |
| Release Date | July 2018                                                       |

### Port status
|         Component | Status | Details                                |
|------------------:|:------:|----------------------------------------|
|          AppArmor |    Y   |                                        |
|      Boot into UI |    Y   |                                        |
|             Wi-Fi |    Y   |                                        |
|         Bluetooth |    Y   |                                        |
|             Sound |    Y   |                                        |
|            Camera |    Y   |                                        |
|   Video Recording |    N   |                                        |
|    Cellular Calls |    Y   |                                        |
|     Cellular Data |    Y   |                                        |
|               GPS |    Y   |                                        |
|           Sensors |    Y   |                                        |
|       Fingerprint |    Y   |                                        |
|          Vibrator |    Y   |                                        |
|             Anbox |    Y   |                                        |
| UBPorts Installer |    N   |                                        |
|  UBPorts Recovery |    N   |                                        |


### Maintainer(s)
shoukolate

### Contributor(s)
nebrassy, robante15

### Source repos
https://github.com/ubports-xiaomi-sdm660

[Telegram chat](https://t.me/shoukolab)
