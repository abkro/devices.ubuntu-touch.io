---
codename: 'sargo'
name: 'Google Pixel 3a'
comment: 'community device'
icon: 'phone'
maturity: .9
---

### Maintainer(s)

- [fredldotme](https://forums.ubports.com/user/fredldotme)

### Forum topic

- https://forums.ubports.com/topic/4621/google-pixel-3a-sargo

#### Kernel

- https://github.com/fredldotme/android_kernel_google_bonito

#### Device

- https://github.com/fredldotme/android_device_google_bonito

#### Halium

- https://github.com/Halium/halium-devices/blob/halium-9.0/manifests/google_sargo.xml

